<?php
/*
Template Name: Services
*/
global $post;
$page_layout = get_post_meta( $post->ID, '_layout', true );
$padding = get_post_meta( $post->ID, '_padding', true );

if ( empty( $page_layout ) ) {
	$page_layout = 'right';
}
$padding = ($padding == 'true') ? 'no-padding' : '';

get_header(); ?>

<div id="splash-feature">
	<?php the_post_thumbnail("full");?>
</div>
<div id="theme-page">
	<div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper <?php echo $page_layout; ?>-layout <?php echo $padding; ?> mk-grid vc_row-fluid">
		<div class="theme-content <?php echo $padding; ?>" itemprop="mainContentOfPage">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post();?>
			<?php the_content();?>
			<div class="clearboth"></div>
			<?php wp_link_pages( 'before=<div id="mk-page-links">'.__( 'Pages:', 'mk_framework' ).'&after=</div>' ); ?>
			<?php endwhile; ?>
		</div>
		<?php //comments_template( '', true );  ?>
		<?php if ( $page_layout != 'full' ) get_sidebar(); ?>
		<div class="clearboth"></div>
	</div>
	<div class="clearboth"></div>
</div>
<?php //do_action( 'footer_twitter' ); removed! ?>
<div data-speedfactor="4" class="full-width-1275  full-height-false mk-video-holder parallax-false mk-page-section mk-blur-parent mk-shortcode  " id="shout-box">
	<div class="mk-video-color-mask"></div>
	<div class="mk-grid vc_row-fluid row-fluid page-section-content">
		<div class="mk-padding-wrapper">
			<div class="vc_span12 wpb_column column_container " style="">
				<div class="wpb_row  vc_row-fluid  mk-fullwidth-false add-padding-0 attched-false">
					<div class="vc_col-sm-6 wpb_column column_container" style="">
						<h2 class="mk-shortcode mk-fancy-title simple-style " id="fancy-title-819" style="font-size: 24px;text-align:right;color: #ffffff;font-weight:normal;margin-top:10px;margin-bottom:18px; letter-spacing:0px;"><span style="">Comselec is a product member of</span></h2>
						<div class="clearboth"></div>
					</div>
					<div class="vc_col-sm-6 wpb_column column_container" style="">
						<div style="max-width: 530px; margin-bottom:10px" class="mk-image-shortcode mk-shortcode   align-left simple-frame inside-image ">
							<div class="mk-image-inner"><img src="http://localhost/comselec/wp-content/uploads/bfi_thumb/accred-2x3gol1itbm8ho56gcckqo.png" title="" alt="" class="lightbox-false"></div>
							<div class="clearboth"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearboth"></div>
	</div>
	<div class="clearboth"></div>
</div>
<?php get_footer(); ?>
